SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `mydb` ;
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`CreditCard`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`CreditCard` ;

CREATE TABLE IF NOT EXISTS `mydb`.`CreditCard` (
  `CNumber` VARCHAR(50) NULL,
  `Type` VARCHAR(15) NULL,
  `HolderName` VARCHAR(25) NULL,
  `ExpirationDate` DATE NULL,
  PRIMARY KEY (`CNumber`),
  UNIQUE INDEX `CNumber_UNIQUE` (`CNumber` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Flow`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Flow` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Flow` (
  `FlowId` INT NOT NULL,
  PRIMARY KEY (`FlowId`),
  UNIQUE INDEX `FlowId_UNIQUE` (`FlowId` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Sets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Sets` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Sets` (
  `SetsId` INT NOT NULL DEFAULT 0,
  `Title` VARCHAR(100) NOT NULL,
  `Description` VARCHAR(500) NULL,
  PRIMARY KEY (`SetsId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Story`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Story` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Story` (
  `StoryId` INT NOT NULL DEFAULT 0,
  `Title` VARCHAR(100) NOT NULL,
  `Content` VARCHAR(1000) NULL,
  `Tags` VARCHAR(100) NULL,
  PRIMARY KEY (`StoryId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Portfolio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Portfolio` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Portfolio` (
  `PortfolioId` INT NOT NULL DEFAULT 0,
  `Title` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`PortfolioId`),
  UNIQUE INDEX `PortfolioId_UNIQUE` (`PortfolioId` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Shipping`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Shipping` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Shipping` (
  `ShippingId` INT NOT NULL AUTO_INCREMENT,
  `Country` VARCHAR(30) NULL,
  `Address` VARCHAR(45) NULL,
  `City` VARCHAR(30) NULL,
  `State` VARCHAR(30) NULL,
  `PostalCode` VARCHAR(30) NULL,
  PRIMARY KEY (`ShippingId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Member`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Member` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Member` (
  `MemberID` INT NOT NULL AUTO_INCREMENT,
  `UserName` VARCHAR(45) NOT NULL,
  `FirstName` VARCHAR(20) NOT NULL,
  `LastName` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(20) NOT NULL,
  `Password` VARCHAR(32) NOT NULL,
  `JoinDate` DATE NOT NULL,
  `Country` VARCHAR(30) NULL,
  `Gender` VARCHAR(6) NULL,
  `About` VARCHAR(1000) NULL,
  `Age` INT NULL,
  `CreditCard_CNumber` VARCHAR(50) NULL,
  `Flow_FlowId` INT NULL,
  `Sets_SetsId` INT NULL,
  `Story_StoryId` INT NULL,
  `Portfolio_PortfolioId` INT NULL,
  `Shipping_ShippingId` INT NULL,
  PRIMARY KEY (`MemberID`),
  UNIQUE INDEX `UserName_UNIQUE` (`UserName` ASC),
  UNIQUE INDEX `Email_UNIQUE` (`Email` ASC),
  INDEX `fk_Member_CreditCard1_idx` (`CreditCard_CNumber` ASC),
  INDEX `fk_Member_Flow1_idx` (`Flow_FlowId` ASC),
  INDEX `fk_Member_Sets1_idx` (`Sets_SetsId` ASC),
  INDEX `fk_Member_Story1_idx` (`Story_StoryId` ASC),
  INDEX `fk_Member_Portfolio1_idx` (`Portfolio_PortfolioId` ASC),
  UNIQUE INDEX `MemberID_UNIQUE` (`MemberID` ASC),
  INDEX `fk_Member_Shipping1_idx` (`Shipping_ShippingId` ASC),
  CONSTRAINT `fk_Member_CreditCard1`
    FOREIGN KEY (`CreditCard_CNumber`)
    REFERENCES `mydb`.`CreditCard` (`CNumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Member_Flow1`
    FOREIGN KEY (`Flow_FlowId`)
    REFERENCES `mydb`.`Flow` (`FlowId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Member_Sets1`
    FOREIGN KEY (`Sets_SetsId`)
    REFERENCES `mydb`.`Sets` (`SetsId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Member_Story1`
    FOREIGN KEY (`Story_StoryId`)
    REFERENCES `mydb`.`Story` (`StoryId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Member_Portfolio1`
    FOREIGN KEY (`Portfolio_PortfolioId`)
    REFERENCES `mydb`.`Portfolio` (`PortfolioId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Member_Shipping1`
    FOREIGN KEY (`Shipping_ShippingId`)
    REFERENCES `mydb`.`Shipping` (`ShippingId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`License`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`License` ;

CREATE TABLE IF NOT EXISTS `mydb`.`License` (
  `Name` VARCHAR(25) NOT NULL DEFAULT 'Royalty Free',
  PRIMARY KEY (`Name`),
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Category` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Category` (
  `CategoryId` INT NOT NULL DEFAULT 0,
  `Name` VARCHAR(20) NULL,
  PRIMARY KEY (`CategoryId`),
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Market`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Market` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Market` (
  `MarketId` INT NOT NULL,
  PRIMARY KEY (`MarketId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Photo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Photo` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Photo` (
  `PhotoID` INT NOT NULL AUTO_INCREMENT,
  `CameraModel` VARCHAR(30) NULL,
  `FocalLength` INT NULL,
  `ShutterSpeed` INT NULL,
  `Aperture` FLOAT NULL,
  `Iso` INT NULL,
  `Date` DATE NULL,
  `MarketSellState` TINYINT(1) NOT NULL DEFAULT False,
  `Pulse` INT NOT NULL,
  `License_Name` VARCHAR(25) NOT NULL DEFAULT 'Royalty Free',
  `Flow_FlowId` INT NULL,
  `Member_MemberID` INT NOT NULL DEFAULT 0,
  `Category_CategoryId` INT NULL,
  `Story_StoryId` INT NULL,
  `Sets_SetsId` INT NULL,
  `Portfolio_PortfolioId` INT NULL,
  `Market_MarketId` INT NULL,
  PRIMARY KEY (`PhotoID`),
  UNIQUE INDEX `PhotoID_UNIQUE` (`PhotoID` ASC),
  INDEX `fk_Photo_License1_idx` (`License_Name` ASC),
  INDEX `fk_Photo_Category1_idx` (`Category_CategoryId` ASC),
  INDEX `fk_Photo_Story1_idx` (`Story_StoryId` ASC),
  INDEX `fk_Photo_Flow1_idx` (`Flow_FlowId` ASC),
  INDEX `fk_Photo_Sets1_idx` (`Sets_SetsId` ASC),
  INDEX `fk_Photo_Portfolio1_idx` (`Portfolio_PortfolioId` ASC),
  INDEX `fk_Photo_Member1_idx` (`Member_MemberID` ASC),
  INDEX `fk_Photo_Market1_idx` (`Market_MarketId` ASC),
  CONSTRAINT `fk_Photo_License1`
    FOREIGN KEY (`License_Name`)
    REFERENCES `mydb`.`License` (`Name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Photo_Category1`
    FOREIGN KEY (`Category_CategoryId`)
    REFERENCES `mydb`.`Category` (`CategoryId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Photo_Story1`
    FOREIGN KEY (`Story_StoryId`)
    REFERENCES `mydb`.`Story` (`StoryId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Photo_Flow1`
    FOREIGN KEY (`Flow_FlowId`)
    REFERENCES `mydb`.`Flow` (`FlowId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Photo_Sets1`
    FOREIGN KEY (`Sets_SetsId`)
    REFERENCES `mydb`.`Sets` (`SetsId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Photo_Portfolio1`
    FOREIGN KEY (`Portfolio_PortfolioId`)
    REFERENCES `mydb`.`Portfolio` (`PortfolioId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Photo_Member1`
    FOREIGN KEY (`Member_MemberID`)
    REFERENCES `mydb`.`Member` (`MemberID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Photo_Market1`
    FOREIGN KEY (`Market_MarketId`)
    REFERENCES `mydb`.`Market` (`MarketId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Favourite`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Favourite` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Favourite` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Photo_PhotoID` INT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `id_UNIQUE` (`Id` ASC),
  INDEX `fk_Favourite_Photo1_idx` (`Photo_PhotoID` ASC),
  CONSTRAINT `fk_Favourite_Photo1`
    FOREIGN KEY (`Photo_PhotoID`)
    REFERENCES `mydb`.`Photo` (`PhotoID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


insert into Category (CategoryId, Name) values (1, 'Nature');
insert into Category (CategoryId, Name) values (2, 'Animal');
insert into Category (CategoryId, Name) values (3, 'Lanscape');
insert into Category (CategoryId, Name) values (4, 'Flower');
insert into Category (CategoryId, Name) values (5, 'Sport');
insert into Category (CategoryId, Name) values (6, 'Black and White');
insert into Category (CategoryId, Name) values (7, 'Retro');
insert into Category (CategoryId, Name) values (8, 'Cityscape');
insert into Category (CategoryId, Name) values (9, 'Lomo');
insert into Category (CategoryId, Name) values (10,'Time Lapse');

insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1212-1221-1121-1234', 'mastercard', 'Angela Harper', '2015-02-15');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1211-1221-1234-2201', 'americanexpress', 'Gary Austin', '2017-02-15');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1212-1221-1121-1264', 'discover', 'Donna Wells', '2016-02-15');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1212-1221-1121-1224', 'americanexpress', 'Louise Schmidt', '2014-02-15');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1212-1221-1121-1364', 'mastercard', 'Brandon Mills', '2016-02-15');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1211-1221-1234-2221', 'discover', 'Steve Kennedy', '2016-02-15');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1211-1221-1434-2201', 'visa', 'Clarence Anderson', '2015-02-15');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1228-1261-1221-1431', 'mastercard', 'Earl Cooper', '2016-02-15');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1234-2121-1221-1211', 'visa', 'Justin Harvey', '2017-05-14');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1211-1271-1234-2201', 'visa', 'Betty Fowler', '2014-02-15');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1234-2181-1221-1211', 'discover', 'Wanda Walker', '2012-02-15');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1211-1291-1234-2201', 'visa', 'Carl Ortiz', '2012-02-15');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1211-1211-1234-2201', 'visa', 'Douglas Castillo', '2012-02-15');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1228-1241-1221-1431', 'americanexpress', 'Robin Cunningham', '2015-02-05');
insert into CreditCard (CNumber, Type, HolderName, ExpirationDate) values ('1212-1251-1121-1234', 'americanexpress', 'Joan Mills', '2016-02-16');

insert into License (Name) values ('Royalty Free');
insert into License (Name) values ('Rights Managed');
insert into License (Name) values ('Enhanced License');

insert into Market (MarketId) values (1);

insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('kperez', 'Victor', 'Kim', 'vkim@worda.info', '3qNFgxT', '2009-02-05', 'Aruba', 'Female', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 58);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('elarson', 'Clarence', 'Myers', 'cmyers@jax.name', 'CgHihF0Pkcdy', '2010-02-05', 'Cuba', 'Female', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 76);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('cgilbert', 'Nicholas', 'Hart', 'nhart@ainyx.biz', 'JDeYzaMCO', '2009-02-09', 'Liberia', 'Female', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 6);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('amendoza', 'Paula', 'Chapman', 'pchapman@feedmix.com', 'k6pucEt8hAnX', '2011-02-05', 'China', 'Male', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 10);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('mwest', 'Lois', 'Price', 'lprice@wikido.edu', 'xmzMUv3', '2013-02-05', 'Belize', 'Male', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 1);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('pwatson', 'Charles', 'Weaver', 'cweaver@oodoo.edu', 'KSdH1s', '2014-02-05', 'Saint Kitts and Nevis', 'Female', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 19);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('lwatson', 'Louise', 'Daniels', 'ldaniels@youopia.edu', 'b235W6', '2009-09-05', 'Åland', 'Male', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 15);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('djordan', 'Patricia', 'Ross', 'pross@talane.name', 'hAqdHA0hX', '2009-12-05', 'Kazakhstan', 'Male', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 75);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('nruiz', 'Raymond', 'Jackson', 'rjackson@dev.mil', 'WoSdEqJpW', '2009-02-05', 'Paraguay', 'Male', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 65);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('jcole', 'Jerry', 'Evans', 'jevans@browse.com', 'ZKoIT8pP3', '2009-02-05', 'Montserrat', 'Female', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', 2);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('tmitchell', 'Shawn', 'Arnold', 'sarnold@blogtags.org', '5LLrug', '2010-02-05', 'Moldova', 'Male', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 2);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('djenkins', 'Steve', 'Carr', 'scarr@shuffletag.gov', '0Ajlvcb0Ny', '2008-02-05', 'Egypt', 'Female', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 30);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('abennett', 'Randy', 'Baker', 'rbaker@meemm.gov', 'gbcNleJRJ', '2009-02-05', 'Cambodia', 'Female', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 53);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('rmendoza', 'Gregory', 'Phillips', 'gphillips@meejo.org', 'nFHfQe8r69M', '2009-02-05', 'Denmark', 'Male', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 42);
insert into Member (UserName, FirstName, LastName, Email, Password, JoinDate, Country, Gender, About, Age) values ('pkelley', 'Jason', 'Ruiz', 'jruiz@photobean.org', 'tmcYeeMrm8', '2011-02-05', 'Western Sahara', 'Male', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 9);

insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('P510', 648.24, 569.93, 4.7, 13025, '2009-11-20', false, 87.51, 1);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('HS20', 329.08, 1558.71, 12.5, 13250, '2010-04-26', false, 77.21, 1);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('p500', 682.85, 2725.12, 9.1, 7161, '2010-01-13', false, 40.15, 1);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('EOS600', 256.97, 3247.59, 15.4, 6098, '2013-02-14', false, 39.65, 1);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('EOS600', 668.63, 2561.54, 8.3, 2284, '2012-02-14', true, 11.38, 1);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('D5000', 41.4, 3617.67, 3.8, 12176, '2012-02-14', false, 49.77, 1);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('D5000', 141.84, 1675.74, 5.5, 6950, '2012-09-14', false, 43.91, 1);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('E26', 749.57, 2183.18, 11.1, 15257, '2012-11-14', true, 43.33, 1);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('D5000', 162.55, 302.34, 1.9, 7382, '2012-07-14', false, 95.84, 1);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('D300', 565.6, 1353.96, 5.4, 13369, '2012-06-14', false, 18.65, 2);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('D5000', 646.57, 1540.31, 3.3, 13992, '2012-05-14', true, 10.37, 2);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('EOS600', 615.5, 3817.81, 11.6, 2346, '2012-04-04', true, 39.98, 2);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('D3', 40.02, 1504.78, 5.7, 11666, '2012-02-01', true, 96.54, 2);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('HS30', 742.92, 3919.37, 5.1, 8011, '2012-02-19', true, 17.23, 2);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('D300', 299.56, 278.46, 8.9, 7684, '2012-02-12', false, 69.05, 2);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('p500', 751.06, 3248.79, 8.3, 9418, '2010-01-14', true, 80.23, 2);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('HS20', 535.64, 3987.46, 11.2, 121, '2011-02-14', false, 12.93, 3);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('E26', 666.21, 1203.56, 12.6, 7911, '2012-03-14', true, 87.5, 3);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('HS20', 583.92, 1237.08, 11.5, 9314, '2011-05-14', false, 55.15, 3);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('HS20', 42.17, 3023.79, 6.2, 14299, '2011-02-14', true, 22.97, 3);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('EOS600', 507.08, 1679.87, 8.8, 8429, '2010-02-14', true, 60.31, 3);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('EOS600', 129.88, 2528.92, 3.5, 10735, '2009-02-14', false, 34.06, 3);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('D5000', 67.45, 1001.95, 1.0, 10344, '2011-02-14', false, 39.26, 4);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('EOS600', 722.55, 2962.78, 6.3, 7866, '2009-02-14', false, 84.48, 4);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('D5000', 60.02, 794.78, 1.1, 6060, '2010-02-14', false, 67.63, 5);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('S4000', 371.83, 453.76, 14.0, 4953, '2010-02-14', true, 11.09, 5);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('D300', 181.78, 1310.18, 1.4, 5753, '2011-02-14', false, 69.8, 7);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('D300', 634.59, 1406.09, 4.8, 1144, '2011-02-14', true, 8.1, 6);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('D5', 253.69, 1002.24, 13.8, 3225, '2009-02-14', true, 63.85, 8);
insert into Photo (CameraModel, FocalLength, ShutterSpeed, Aperture, Iso, Date, MarketSellState, Pulse, Member_MemberID) values ('HS50', 298.03, 1817.99, 4.6, 6862, '2010-02-14', true, 42.45, 7);

insert into Portfolio (PortfolioId, Title) values (1, 'A New Day');
insert into Portfolio (PortfolioId, Title) values (2, 'The New age');
insert into Portfolio (PortfolioId, Title) values (3, 'Summershots');
insert into Portfolio (PortfolioId, Title) values (4, 'A New Day');
insert into Portfolio (PortfolioId, Title) values (5, 'A New Year');
insert into Portfolio (PortfolioId, Title) values (6, 'The New Beginning');
insert into Portfolio (PortfolioId, Title) values (7, 'My Portfolio');
insert into Portfolio (PortfolioId, Title) values (8, 'Snapshots');
insert into Portfolio (PortfolioId, Title) values (9, 'A New Dawn');
insert into Portfolio (PortfolioId, Title) values (10, 'My Portfolio');

insert into Sets (SetsId, Title, Description) values (1, 'Crack Of Dawn', 'com sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus');
insert into Sets (SetsId, Title, Description) values (2, '2Day at the Park', 'lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed');
insert into Sets (SetsId, Title, Description) values (3,'Sunny Morning', 'non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus');
insert into Sets (SetsId, Title, Description) values (4,'The New Beginning', 'amet justo morbi ut odio');
insert into Sets (SetsId, Title, Description) values (5, 'Big Day Out', 'amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet');
insert into Sets (SetsId, Title, Description) values (6, 'Sunny Morning', 'amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula');
insert into Sets (SetsId, Title, Description) values (7, 'Concert Snapshots', 'morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl');
insert into Sets (SetsId, Title, Description) values (8, 'Sunny Morning', 'erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat');
insert into Sets (SetsId, Title, Description) values (9, 'Concert Snapshots', 'ipsum primis in faucibus orci');
insert into Sets (SetsId, Title, Description) values (10, 'The New Beginning', 'facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque');

insert into Shipping (Country, Address, City, State, PostalCode) values ('Estonia', '23098 Toban Alley', 'Paso Robles', 'Arkansas', '38568-8311');
insert into Shipping (Country, Address, City, State, PostalCode) values ('Monaco', '353 Ilene Hill', 'Temple City', 'Rhode Island', '04825-9051');
insert into Shipping (Country, Address, City, State, PostalCode) values ('Luxembourg', '0565 Oneill Circle', 'Pittsburg', 'Nebraska', '31350');
insert into Shipping (Country, Address, City, State, PostalCode) values ('Kenya', '5802 GulSetsh Center', 'Watsonville', 'Wyoming', '53796');
insert into Shipping (Country, Address, City, State, PostalCode) values ('Mexico', '10 Westridge Park', 'Bishop', 'Minnesota', '44248-9143');
insert into Shipping (Country, Address, City, State, PostalCode) values ('British Virgin Islands', '256 Declaration Place', 'Burbank', 'Maryland', '40410-6488');
insert into Shipping (Country, Address, City, State, PostalCode) values ('French Southern Territories', '03 Debs Trail', 'Oakley', 'South Dakota', '14991');
insert into Shipping (Country, Address, City, State, PostalCode) values ('United Arab Emirates', '68 Cody Place', 'Hermosa Beach', 'Oregon', '98463');
insert into Shipping (Country, Address, City, State, PostalCode) values ('Iceland', '1 Reindahl Alley', 'Sunnyvale', 'Pennsylvania', '14680-5621');
insert into Shipping (Country, Address, City, State, PostalCode) values ('Uganda', '3 Becker Trail', 'Corona', 'Nebraska', '93736-8740');
insert into Shipping (Country, Address, City, State, PostalCode) values ('Qatar', '1 Memorial Circle', 'La Verne', 'Oregon', '69495-0570');
insert into Shipping (Country, Address, City, State, PostalCode) values ('Andorra', '9689 Cardinal Circle', 'South Pasadena', 'Montana', '01338');
insert into Shipping (Country, Address, City, State, PostalCode) values ('Botswana', '0 4th Circle', 'Paramount', 'Washington', '24103-1157');
insert into Shipping (Country, Address, City, State, PostalCode) values ('Suriname', '954 Sundown Hill', 'Rancho Mirage', 'Kentucky', '95043');
insert into Shipping (Country, Address, City, State, PostalCode) values ('Cameroon', '909 Summerview Way', 'Morro Bay', 'Maine', '39511-6713');

insert into Story (StoryId, Title, Content, Tags) values (1, 'massa id', 'morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet', 'vitae');
insert into Story (StoryId, Title, Content, Tags) values (2, 'turpis enim blandit mi', 'donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla', 'est congue elementum in hac habitasse platea dictumst morbi vestibulum');
insert into Story (StoryId, Title, Content, Tags) values (3, 'tristique fusce', 'cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum', 'duis bibendum');
insert into Story (StoryId, Title, Content, Tags) values (4, 'sem fusce consequat nulla', 'mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus', 'justo pellentesque viverra pede ac diam cras pellentesque volutpat');
insert into Story (StoryId, Title, Content, Tags) values (5, 'placerat ante nulla', 'tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula', 'sit amet turpis');
insert into Story (StoryId, Title, Content, Tags) values (6, 'platea dictumst maecenas ut', 'bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus', 'lacus at velit vivamus vel nulla eget eros');
insert into Story (StoryId, Title, Content, Tags) values (7, 'integer tincidunt ante vel ipsum', 'vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero', 'donec');
insert into Story (StoryId, Title, Content, Tags) values (8, 'curae mauris viverra diam', 'nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum', 'parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum');
insert into Story (StoryId, Title, Content, Tags) values (9, 'nascetur ridiculus mus vivamus', 'proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra', 'vestibulum sed magna at');
insert into Story (StoryId, Title, Content, Tags) values (10,'at dolor', 'vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac', 'sapien varius ut');
insert into Story (StoryId, Title, Content, Tags) values (11, 'sodales scelerisque mauris', 'nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis', 'tortor duis mattis');
insert into Story (StoryId, Title, Content, Tags) values (12, 'justo eu massa', 'aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes', 'dui vel nisl duis ac');
insert into Story (StoryId, Title, Content, Tags) values (13, 'neque sapien placerat ante nulla', 'volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet', 'convallis morbi odio');
insert into Story (StoryId, Title, Content, Tags) values (14, 'porttitor pede justo', 'suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper', 'vestibulum ac est lacinia nisi venenatis tristique');
insert into Story (StoryId, Title, Content, Tags) values (15, 'pede malesuada', 'lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio', 'cum');

insert into Flow  (FlowId) values (1);
insert into Flow  (FlowId) values (2);
insert into Flow  (FlowId) values (3);
insert into Flow  (FlowId) values (4);
insert into Flow  (FlowId) values (5);
insert into Flow  (FlowId) values (6);
insert into Flow  (FlowId) values (7);
insert into Flow  (FlowId) values (8);
insert into Flow  (FlowId) values (9);
insert into Flow  (FlowId) values (10);
insert into Flow  (FlowId) values (11);
insert into Flow  (FlowId) values (12);
insert into Flow  (FlowId) values (13);
insert into Flow  (FlowId) values (14);
insert into Flow  (FlowId) values (15);
insert into Flow  (FlowId) values (16);

SELECT * FROM Story;
SELECT * FROM Member WHERE Age > 15;
SELECT DISTINCT FROM Category; 